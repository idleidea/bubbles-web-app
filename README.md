# Bubbles


## overview
Our society lacks an ability to conceive of commonalities between themselves and other Australians. Additionally, data, despite its abundance, is not currently used by the Government to tell engaging, short-form narratives ready for social-media consumption.

Why is this a problem? If people cannot understand the data and draw linkages to their fellow Australians, then they will lack the empathy for supporting government policies that benefit our country as a whole. 

This static webpage serves as an example of what we can present to the public in order to expose people to information which may be new to them in an engaging and emotive way.